# Sample App
Sample app deploys a simple API along with a mysql database and creates/updates the clients.

## Dependencies
- Gitlab CI

## References
- https://bitnami.com/stack/mysql/helm
- https://hub.docker.com/r/dtzar/helm-kubectl/
- https://pipenv.pypa.io/en/latest/
- https://6ff605ef23.interview.vme.dev/swagger

## Quickstart
Make sure your values are set properly in `mysql-values.yml` and run a pipeline.
The `onboard.py` script will create the clients based on the `clients.csv` and update any existing with the same ID.