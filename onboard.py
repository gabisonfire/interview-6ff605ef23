import csv
import requests

URL = "https://6ff605ef23.interview.vme.dev/api"
HEADERS = {"Content-Type":"application/json"}

with open("clients.csv") as f:
    clients = csv.DictReader(f)
    for client in clients:
        try:
            response = requests.post(f"{URL}/v1/client", json=client, headers=HEADERS)
        except requests.exceptions.RequestException as e:
            print(f"Error making request for {client['name']}: {e}")
        if response.status_code == 201:
            print(f"Client {client['name']} added.")
        elif response.status_code == 409:
            print(f"Client {client['name']} already exists, updating.")
            try:
                response = requests.put(f"{URL}/v1/client", json=client, headers=HEADERS)
            except requests.exceptions.RequestException as e:
                print(f"Error making request for {client['name']}: {e}")
            if response.status_code == 200:
                print(f"Client {client['name']} was updated.")
            elif response.status_code == 304:
                print(f"Client {client['name']} was already up-to-date.")
            else:
                print(f"Error: ({client['name']}) {response.status_code}: {response.text}.")
        else:
            print(f"Error: ({client['name']}) {response.status_code}: {response.text}.")